Project-RJ
==========

Introduction
------------

Project-RJ is a free and open source 3rd person 3D RPG video game
project written in pure rust. It is a very personal toy. Nothing
essential. You can just simply ignore it and leave.

Installation
------------

Install Rust toolchain first (https://rustup.rs/), then:

Clone the latest fyrox game engine:

    git clone https://github.com/FyroxEngine/Fyrox

Clone the game:

    git clone https://gitlab.com/iceguye/project-rj

Go to the game's directory:

    cd project-rj

To play the game:

    cargo run --package executor --release

To edit and test the game:

    cargo run --package editor --release

Copyright
---------

Unless otherwise noted, all files in this project, including but not
limited to source codes and art contents, are free software: you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, version 3
of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.