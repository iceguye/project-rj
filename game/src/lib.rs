//! Game project.
pub mod player_control;
pub mod door_open;
pub mod utilities;
pub mod ability;
pub mod arrow_shoot_out;
pub mod prj_message;
pub mod key_pressed;

use fyrox::{
    core::{pool::Handle},
    event::{Event, ElementState, WindowEvent},
    keyboard::{KeyCode, PhysicalKey},
    gui::message::UiMessage,
    plugin::{Plugin, PluginConstructor, PluginContext,
             PluginRegistrationContext},
    scene::{Scene},
    window::{CursorGrabMode,
             Fullscreen,
             Fullscreen::{Exclusive, Borderless},
             Window},
    dpi::PhysicalSize,
};

use std::path::Path;
//use std::process::exit;
use player_control::PlayerControl;
use door_open::DoorOpen;
use arrow_shoot_out::ArrowShootOut;
use key_pressed::KeyPressed;

pub struct GameConstructor;

impl PluginConstructor for GameConstructor {
    fn register(&self, context: PluginRegistrationContext) {
        // Register your scripts here.
        context.serialization_context.script_constructors
            .add::<PlayerControl>("Player Control");
        context.serialization_context.script_constructors
            .add::<DoorOpen>("Door Open");
        context.serialization_context.script_constructors
            .add::<ArrowShootOut>("Arrow Shoot Out");
    }

    fn create_instance(
        &self,
        scene_path: Option<&str>,
        context: PluginContext,
    ) -> Box<dyn Plugin> {
        Box::new(Game::new(scene_path, context))
    }
}

pub struct Game {
    scene: Handle<Scene>,
    main_menu_on: bool,
    menu_on: bool,
    key_pressed: KeyPressed,
}

impl Game {
    pub fn new(scene_path: Option<&str>, context: PluginContext) -> Self {
        context
            .async_scene_loader
            .request(scene_path.unwrap_or("data/scene.rgs"));
        let menu_on = false;
        let main_menu_on = false;
        let key_pressed = KeyPressed {..Default::default()};
        Self { scene: Handle::NONE,
               main_menu_on,
               menu_on,
               key_pressed}
    }

    pub fn fullscreen_size(&self, window: &Window) -> PhysicalSize<u32> {
        let fullscreen = window.fullscreen();
        if fullscreen.is_some() {
            match fullscreen.unwrap() {
                Exclusive(vm) => {
                    vm.size()
                },
                Borderless(mh) => {
                    if mh.is_some() {
                        mh.unwrap().size()
                    } else {
                        println!("Borderless(None)");
                        PhysicalSize::new(0, 0)
                    }
                }
            }
        } else {
            println!("It's not fullscreen.");
            PhysicalSize::new(0, 0)
        }
    }

    pub fn toggle_fullscreen(&mut self, context: PluginContext) {
        let window = &context.graphics_context.as_initialized_mut().window;
        let check_fullscreen = window.fullscreen();
        if check_fullscreen.is_none() {
            window.set_fullscreen(Some(Fullscreen::Borderless(
                window.current_monitor())));
        } else {
            window.set_fullscreen(None);
        }
    }

    pub fn toggle_menu(&mut self, context: PluginContext) {
        let window = &context.graphics_context.as_initialized_mut().window;
        if self.main_menu_on == false && self.menu_on == false {
            window.set_cursor_visible(true);
            window.set_cursor_grab(CursorGrabMode::None).unwrap();
            self.menu_on = true;
        } else if self.main_menu_on == false {
            window.set_cursor_visible(false);
            window.set_cursor_grab(CursorGrabMode::Confined)
                .or_else(|_e|
                         window.set_cursor_grab(CursorGrabMode::Locked))
                .unwrap();
            self.menu_on = false;
        }
    }

    pub fn key_shortcut_action(&mut self,
                               context: PluginContext,
                               window_event: &WindowEvent) {
        match window_event {
            WindowEvent::ModifiersChanged(modifier) => {
                self.key_pressed.alt = modifier.state().alt_key();
            },
            WindowEvent::KeyboardInput {event, ..} => {
                let phy_key = event.physical_key;
                let key_state = event.state;
                match phy_key {
                    PhysicalKey::Code(key_code) => {
                        match key_state {
                            ElementState::Pressed => {
                                match key_code {
                                    KeyCode::KeyM => {
                                        if self.key_pressed.m == false {
                                            self.toggle_menu(context);
                                        }
                                        self.key_pressed.m = true;
                                    },
                                    KeyCode::Enter => {
                                        if self.key_pressed.alt
                                            && self.key_pressed.enter == false
                                        {
                                            self.key_pressed.enter = true;
                                            self.toggle_fullscreen(context);
                                        }
                                    },
                                    _=> {},
                                }
                            },
                            _=> {
                                match key_code {
                                    KeyCode::KeyM => {
                                        self.key_pressed.m = false;
                                    },
                                    KeyCode::Enter => {
                                        self.key_pressed.enter = false;
                                    },
                                    _=> {},
                                }
                            },
                        }
                    },
                    _=> {},
                }
            },
            _=> {},
        }
    }
}

impl Plugin for Game {
    fn on_deinit(&mut self, _context: PluginContext) {
        // Do a cleanup here.
    }

    fn on_graphics_context_initialized(
        &mut self,
        context: PluginContext<'_, '_>,) {
        let window = &context.graphics_context.as_initialized_mut().window;
        window.set_title("Project-RJ");
        window.set_cursor_visible(false);
        window.set_cursor_grab(CursorGrabMode::Confined)
            .or_else(|_e|
                     window.set_cursor_grab(CursorGrabMode::Locked))
            .unwrap();
        // window.set_fullscreen(Some(Fullscreen::Borderless(
        //     window.current_monitor())));
    }

    fn update(&mut self,
              _context: &mut PluginContext,) {
        // Add your global update code here.
    }

    fn on_os_event(
        &mut self,
        event: &Event<()>,
        context: PluginContext,
    ) {
        // Do something on OS event here.
        match event {
            Event::WindowEvent { event, .. } => {
                self.key_shortcut_action(context, event);
            },
            _=> {}
        }
    }

    fn on_ui_message(
        &mut self,
        _context: &mut PluginContext,
        _message: &UiMessage,
    ) {
        // Handle UI events here.
    }

    fn on_scene_begin_loading(&mut self,
                              _path: &Path,
                              ctx: &mut PluginContext) {
        if self.scene.is_some() {
            ctx.scenes.remove(self.scene);
        }
    }

    fn on_scene_loaded(
        &mut self,
        _path: &Path,
        scene: Handle<Scene>,
        _data: &[u8],
        _context: &mut PluginContext,
    ) {    
        self.scene = scene;
    }
}
