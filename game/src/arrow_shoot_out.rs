//  Copyright © 2023 IceGuye.

//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, version 3 of the
//  License.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <http://www.gnu.org/licenses/>.

use fyrox::{
    core::{visitor::prelude::*,
           reflect::prelude::*,
           type_traits::prelude::*,
           pool::Handle,
           algebra::{UnitQuaternion, Vector3, Point3},
           arrayvec::ArrayVec,
    },
    event::Event,
    scene::{node::{Node},
            graph::{Graph,
                    physics::{Intersection,
                              RayCastOptions,
                    },
            },
            Scene,
    },
    script::{ScriptContext,
             ScriptDeinitContext,
             ScriptTrait,
             ScriptMessagePayload,
             ScriptMessageContext},
};

use crate::utilities::distance_3d;
use crate::prj_message::{ShootingMsg};

#[derive(Visit, Reflect, Default, Debug, Clone)]
pub struct ArrowFilter {
    filter_list: Vec<String>,
}

impl ArrowFilter {
    fn new() -> ArrowFilter {
        ArrowFilter {
            filter_list: Vec::new(),
        }
    }
    
    fn check_filter(&self, name: &str) -> bool {
        let mut result = false;
        for i in self.filter_list.iter() {
            if i == &String::from(name) {
                result = true;
                break;
            }
        }
        result
    }
    
    fn add(&mut self, name: &str) {
        self.filter_list.push(String::from(name));
    }

    fn append(&mut self, filter_slice: &[&str]) {
        for i in filter_slice.iter() {
            self.add(i);
        }
    }

    fn _remove(&mut self, name: &str) {
        let name = String::from(name);
        let mut i = 0;
        while i < self.filter_list.len() {
            if self.filter_list[i] == name {
                self.filter_list.remove(i);
            } else {
                i = i + 1;
            }
        }
    }
}

#[derive(Visit, Reflect, Default, Debug, Clone,
         TypeUuidProvider, ComponentProvider)]
#[type_uuid(id = "24dcc585-753a-4339-8390-f521612c5cd3")]
#[visit(optional)]
pub struct ArrowShootOut {
    // Add fields here.
    ray_cast_begin: Vector3<f32>,
    ray_cast_end: Handle<Node>,
    shooting_sound: Handle<Node>,
    hitting_sound: Handle<Node>,
    receive_shooting_msg: bool, 
    arrow_yaw: f32,
    arrow_pitch: f32,
    init_spd: f32,
    spd_v: f32,
    spd_h: f32,
    is_flying: bool,
    arrow_filter: ArrowFilter,
}

impl ArrowShootOut {
    fn new(&mut self,
           graph: &mut Graph,
           handle: Handle<Node>,) -> ArrowShootOut {
        let ray_cast_end = graph.find_by_name(handle, "Ray Cast End")
            .unwrap().0;
        let shooting_sound = graph
            .find_by_name(handle, "Arrow Shooting Sound")
            .unwrap().0;
        let hitting_sound = graph
            .find_by_name(handle, "Arrow Hitting Sound")
            .unwrap().0;
        let mut arrow_filter = ArrowFilter::new();
        arrow_filter.append(&["Arrow Collider",
                              "Test 1",
                              "Test 2", ]);
        
        let arrow_shoot_out: ArrowShootOut = ArrowShootOut {
            ray_cast_begin: Vector3::new(0.0, 0.0, 0.0),
            ray_cast_end: ray_cast_end,
            shooting_sound: shooting_sound,
            hitting_sound: hitting_sound,
            receive_shooting_msg: true,
            arrow_yaw: 0.0,
            arrow_pitch: 0.0,
            init_spd: 0.0,
            spd_v: 0.0,
            spd_h: 0.0,
            is_flying: false,
            arrow_filter: arrow_filter,
        };
        arrow_shoot_out
    }

    fn off_string(&mut self,
                  scene: &mut Scene,
                  handle: Handle<Node>,) {
        let graph = &mut scene.graph;
        self.ray_cast_begin = graph[self.ray_cast_end].global_position();
        let arrow_pos = graph[handle].global_position();
        self.ray_cast_begin = graph[handle].global_position();
        graph.unlink_node(handle);
        graph[handle].local_transform_mut().set_position(arrow_pos);
        let multiplied_quaternion_angle =
            UnitQuaternion::from_axis_angle(
                &Vector3::y_axis(),
                360.0f32.to_radians() - self.arrow_yaw,
            ) 
            * UnitQuaternion::from_axis_angle(
                &Vector3::x_axis(),
                self.arrow_pitch,
            );
        graph[handle]
            .local_transform_mut()
            .set_rotation(multiplied_quaternion_angle);
        graph[self.shooting_sound].as_sound_mut().play();
    }

    fn flying(&mut self,
              graph: &mut Graph, handle: Handle<Node>, dt: f32) {
        if self.is_flying {
            let spd_x = self.spd_h * self.arrow_yaw.sin();
            let spd_z = -self.spd_h * self.arrow_yaw.cos();
            let mut arrow_pos = *graph[handle]
                .local_transform().position().get_value_ref();
            // println!("{:?}", arrow_pos);
            arrow_pos[0] = arrow_pos[0] + spd_x * dt;
            arrow_pos[2] = arrow_pos[2] + spd_z * dt;
            let g = -graph.physics.gravity[1];
            self.spd_v -= g * dt;
            arrow_pos[1] = arrow_pos[1] + self.spd_v * dt;
            graph[handle]
                .as_rigid_body_mut()
                .local_transform_mut()
                .set_position(arrow_pos);
            self.arrow_pitch = (self.spd_v / self.spd_h).tan();
            let multiplied_quaternion_angle =
                UnitQuaternion::from_axis_angle(
                    &Vector3::y_axis(),
                    360.0f32.to_radians() - self.arrow_yaw,
                )
                * UnitQuaternion::from_axis_angle(
                    &Vector3::x_axis(),
                    self.arrow_pitch,
                );
            graph[handle]
                .as_rigid_body_mut()
                .local_transform_mut()
                .set_rotation(multiplied_quaternion_angle);

            let mut rc_buf = ArrayVec::<Intersection, 32>::new();
            let ray_begin = self.ray_cast_begin;
            let ray_end = graph[self.ray_cast_end]
                .global_position();
            let ray_dir = ray_end - ray_begin;
            graph.physics.cast_ray(
                RayCastOptions {
                    ray_origin: Point3::from(ray_begin),
                    ray_direction: ray_dir,
                    max_len: distance_3d(ray_end, ray_begin),
                    groups: Default::default(),
                    sort_results: true,
                },
                &mut rc_buf,
            );
            for insct in rc_buf {
                if self.arrow_filter.check_filter(
                    graph[insct.collider].name()) == false {
                    // println!("{}",
                    //          graph[graph[insct.collider].parent()].name());
                    // println!("Ray Begin {:?}", ray_begin);
                    // println!("Ray End   {:?}", ray_end);
                    self.is_flying = false;
                    let insct_pos = Vector3::new(insct.position[0],
                                                 insct.position[1],
                                                 insct.position[2],);
                    let insct_dist = distance_3d(insct_pos, ray_end);
                    if insct_dist > 0.7 {
                        graph[handle]
                            .as_rigid_body_mut()
                            .local_transform_mut()
                            .set_position(insct_pos);
                    }
                    let arrow_pos = graph[handle].global_position();
                    let arrow_collider = graph.find_by_name(handle,
                                                            "Arrow Collider")
                        .unwrap().0;
                    graph.unlink_node(arrow_collider);
                    graph[arrow_collider]
                        .local_transform_mut()
                        .set_position(arrow_pos);
                    
                    graph.link_nodes_keep_global_position_rotation(
                        arrow_collider,
                        graph[insct.collider].parent());
                    graph.remove_node(handle);
                    graph[self.hitting_sound].as_sound_mut().play();
                    break;
                }
            }
            self.ray_cast_begin = ray_end;
            // println!("{}", graph[graph[rc_buf[0].collider].parent()].name());
        }
    }
}

impl ScriptTrait for ArrowShootOut {
    fn on_init(&mut self, context: &mut ScriptContext) {
        // Put initialization logic here.
        *self = self.new(&mut context.scene.graph,
                         context.handle,);
    }

    fn on_start(&mut self, context: &mut ScriptContext) {
        // There should be a logic that depends on other scripts in scene.
        // It is called right after **all** scripts were initialized.
        context.message_dispatcher.subscribe_to::<ShootingMsg>(context.handle);
    }

    fn on_deinit(&mut self, _context: &mut ScriptDeinitContext) {
        // Put de-initialization logic here.
    }

    fn on_os_event(&mut self,
                   _event: &Event<()>,
                   _context: &mut ScriptContext) {
        // Respond to OS events here.
    }

    fn on_update(&mut self, context: &mut ScriptContext) {
        // Put object logic here.
        self.flying(&mut context.scene.graph, context.handle, context.dt);
    }

    fn on_message(
        &mut self,
        message: &mut dyn ScriptMessagePayload,
        context: &mut ScriptMessageContext,
    ) {
        // React to message.
        let shooting_msg = message.downcast_ref::<ShootingMsg>();
        if shooting_msg.is_some() {
            match shooting_msg.unwrap() {
                ShootingMsg::ShootingData {arrow_yaw,
                                           arrow_pitch,
                                           init_spd} => {
                    if self.receive_shooting_msg {
                        self.receive_shooting_msg = false;
                        // println!("ShootingMsg Received: {}, {}, {}",
                        //         arrow_yaw, arrow_pitch, init_spd);
                        self.arrow_yaw = *arrow_yaw;
                        self.arrow_pitch = *arrow_pitch;
                        self.init_spd = *init_spd;
                        self.spd_v = self.init_spd * self.arrow_pitch.sin();
                        self.spd_h = self.init_spd * self.arrow_pitch.cos();
                        self.off_string(&mut context.scene,
                                        context.handle,);
                        self.is_flying = true;
                    }
                },
                _=>{},
            }
        }
    }
}
    
