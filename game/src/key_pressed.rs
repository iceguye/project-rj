use fyrox::{
    core::{reflect::prelude::*,
           visitor::prelude::*,
    },
};

#[derive(Visit, Reflect, Default, Debug, Clone)]
pub struct KeyPressed {
    pub key1: bool,
    pub key2: bool,
    pub key3: bool,
    pub w: bool,
    pub a: bool,
    pub s: bool,
    pub d: bool,
    pub e: bool,
    pub m: bool,
    pub enter: bool,
    pub alt: bool,
}
