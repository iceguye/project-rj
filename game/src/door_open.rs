//  Copyright © 2022 - 2023 IceGuye.

//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, version 3 of the
//  License.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <http://www.gnu.org/licenses/>.

use fyrox::{
    core::{visitor::prelude::*,
           reflect::prelude::*,
           type_traits::prelude::*,
           pool::Handle,},
    event::{Event,
            WindowEvent,
            ElementState},
    keyboard::{KeyCode, PhysicalKey},
    scene::{node::{Node},
            graph::Graph,
            animation::{AnimationPlayer,
                        Animation}
    },
    script::{ScriptContext, ScriptDeinitContext, ScriptTrait},
};

use crate::Game;
use crate::key_pressed::{KeyPressed};

#[derive(Visit, Reflect, Default, Debug, Clone,
         TypeUuidProvider, ComponentProvider)]
#[type_uuid(id = "81a3beb8-8a11-4aaf-9e65-c602dd78ed28")]
#[visit(optional)]
pub struct DoorOpen {
    // Add fields here.
    key_pressed: KeyPressed,
    is_open: bool,
    can_touch: bool,
}

impl DoorOpen{
    fn new(&mut self, handle: Handle<Node>, graph: &mut Graph) -> DoorOpen {
        let door_open_ani = self.get_door_animation(handle, graph);
        door_open_ani.set_loop(false);
        door_open_ani.set_speed(1.0);
        let is_open: bool;
        if door_open_ani.is_enabled() {
            is_open = true;
        } else {
            is_open = false;
        }
        DoorOpen {
            key_pressed: KeyPressed {..Default::default()},
            is_open: is_open,
            can_touch: false,
        }
    }

    fn get_door_animation<'a>(&'a mut self,
                              handle: Handle<Node>,
                              graph: &'a mut Graph)
                              -> &mut Animation {
        let ani_player_handle = graph.find_by_name(handle, "AnimationPlayer")
            .unwrap().0;
        let ani_player = graph[ani_player_handle]
            .query_component_mut::<AnimationPlayer>().unwrap();
        let ani_container = ani_player.animations_mut().get_value_mut_silent();
        let ani_tuple = ani_container.find_by_name_mut("door_open")
            .unwrap();
        let door_open_ani = ani_tuple.1;
        door_open_ani
    }

    fn check_touch_door(&mut self, handle: Handle<Node>, graph: &Graph) {
        let door_collider = graph[handle].as_collider();
        let dc_intersects = door_collider.intersects(&graph.physics);
        let mut can_touch = false;
        for intersect in dc_intersects {
            let sensor_name_1 = graph[intersect.collider1].name();
            let sensor_name_2 = graph[intersect.collider2].name();
            if sensor_name_1 == "Hand Touch Collider"
                || sensor_name_2 == "Hand Touch Collider"
            {
                can_touch = true;
            }
        }
        self.can_touch = can_touch;
    }

    fn toggle_open_close(&mut self, handle: Handle<Node>, graph: &mut Graph) {
        if self.is_open == false {
            if self.can_touch {
                let door_open_ani = self.get_door_animation(handle, graph);
                door_open_ani.set_enabled(true);
                door_open_ani.set_speed(1.0);
                self.is_open = true;
            }
        } else {
            if self.can_touch {
                let door_open_ani = self.get_door_animation(handle, graph);
                door_open_ani.set_enabled(true);
                door_open_ani.set_speed(-1.0);
                self.is_open = false;
            }
        }
    }

    fn key_for_toggle(&mut self,
                      handle: Handle<Node>,
                      graph: &mut Graph,
                      event: &WindowEvent) {
        match event {
            WindowEvent::KeyboardInput {event, ..} => {
                let phy_key = event.physical_key;
                let key_state = event.state;
                match phy_key {
                    PhysicalKey::Code(keycode) => {
                        match keycode {
                            KeyCode::KeyE => {
                                match key_state {
                                    ElementState::Pressed => {
                                        if self.key_pressed.e == false {
                                            self.toggle_open_close(handle,
                                                                   graph);
                                        }
                                        self.key_pressed.e = true;
                                    },
                                    _=>{
                                        self.key_pressed.e = false;
                                    },
                                }
                            },
                            _=>{},
                        }
                    },
                    _=> {},
                }
            },
            _=> {},
        }
        
    }
}

impl ScriptTrait for DoorOpen {
    fn on_init(&mut self, context: &mut ScriptContext) {
        // Put initialization logic here.
        *self = self.new(context.handle, &mut context.scene.graph);
    }

    fn on_start(&mut self, _context: &mut ScriptContext) {
        // There should be a logic that depends on other scripts in scene.
        // It is called right after **all** scripts were initialized.
    }

    fn on_deinit(&mut self, _context: &mut ScriptDeinitContext) {
        // Put de-initialization logic here.
    }

    fn on_os_event(&mut self, event: &Event<()>, context: &mut ScriptContext) {
        // Respond to OS events here.
        match event {
            Event::WindowEvent {event, .. } => {
                let game_plugin = context.plugins[0].cast::<Game>().unwrap();
                let menu_on = game_plugin.menu_on;
                if menu_on == false {
                    self.key_for_toggle(context.handle,
                                        &mut context.scene.graph,
                                        event);
                }
            },
            _=> {}
        }
    }

    fn on_update(&mut self, context: &mut ScriptContext) {
        // Put object logic here.
        self.check_touch_door(context.handle, &context.scene.graph);
    }
}
    
