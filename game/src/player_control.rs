//  Copyright © 2022 - 2023 IceGuye.

//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, version 3 of the
//  License.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <http://www.gnu.org/licenses/>.

use fyrox::{
    core::{reflect::prelude::*,
           visitor::prelude::*,
            type_traits::prelude::*,
           reflect::Reflect,
           pool::Handle,
           algebra::{UnitQuaternion, Vector3, Point3},
           arrayvec::ArrayVec,
           rand::random,
    },
    event::{Event,
            DeviceEvent,
            WindowEvent,
            ElementState,
            MouseButton},
    keyboard::{KeyCode, PhysicalKey},
    scene::{node::{Node},
            graph::{Graph,
                    physics::{Intersection,
                              RayCastOptions,
                    },
            },
            Scene,
    },
    script::{ScriptContext,
             ScriptDeinitContext,
             ScriptTrait,
             ScriptMessageSender},
    asset::manager::ResourceManager,
    resource::model::{ModelResourceExtension, ModelResource}
};
use std::sync::{Arc, Mutex};
use std::{thread};
use crate::utilities::{vec_to_rad,
                       vec_to_rad_f64,
                       distance_2d,
                       normalize_2d,
                       distance_3d,
                       angle_vec_3d,
                       unix_time_secs,
                       projectile_angle,
                       inch_meter,
                       meter_inch,
                       lbs_newton,
                       newton_lbs};

use crate::ability::{shooting_ability_loop};
use crate::prj_message::{ShootingMsg};
use crate::Game;
use crate::key_pressed::{KeyPressed};

#[derive(Visit, Reflect, Default, Debug, Clone)]
pub enum CombatMode {
    #[default]
    NonCombat,
    Archery,
    Melee,
}

#[derive(Visit, Reflect, Default, Debug, Clone)]
pub enum PrepareMeleeAttackFrom {
    #[default]
    TopRight,
    TopLeft,
    BottomRight,
    BottomLeft,
    Top,
    Back,
}
    
#[derive(Visit, Reflect, Default, Debug, Clone,
         TypeUuidProvider, ComponentProvider)]
#[type_uuid(id = "9b320ad6-d124-44b4-a8d8-511e628a7131")]
#[visit(optional)]
pub struct PlayerControl {
    // Add fields here.
    mouse_sensitivity: f32,
    mouse_vir_x_0: f64,
    mouse_vir_y_0: f64,
    mouse_vir_x: f64,
    mouse_vir_y: f64,
    key_pressed: KeyPressed,
    combat_mode: CombatMode,
    cam_pitch: f32,
    cam_yaw: f32,
    camera_root: Handle<Node>,
    camera_pivot_1: Handle<Node>,
    camera_pivot_2: Handle<Node>,
    auto_aim_cone: Handle<Node>,
    auto_aim_ray_end: Handle<Node>,
    player: Handle<Node>,
    player_yaw: f32,
    yaw_want_in_vec: [f32; 2],
    yaw_want: f32,
    walk_speed: f32,
    target_to_aim: Option<Handle<Node>>,
    arrow_anchor: Handle<Node>,
    preloaded_arrow: Option<ModelResource>,
    yaw_to_aim: f32,
    focus: bool,
    bow_weight_snd: Arc<Mutex<f64>>,
    current_draw_weight_snd: Arc<Mutex<f64>>,
    last_draw_time_snd: Arc<Mutex<f64>>,
    averaged_draw_weight_rcv: Arc<Mutex<f64>>,
    avg_hold_over_rcv: Arc<Mutex<f64>>,
    current_hold_over_snd: Arc<Mutex<f64>>,
    bow_start_drawing: bool,
    current_draw_length: f64,
    shooting_msg: ShootingMsg,
    bow_reload_timer: f32,
    bow_reload_timer_start: bool,
    pre_melee_attack_from: PrepareMeleeAttackFrom,
}

impl PlayerControl{
    fn new(&mut self,
           graph: &mut Graph,
           resource_manager: &ResourceManager) -> PlayerControl {
        let camera_root = graph
            .find_by_name_from_root("Camera Root").unwrap().0;
        let camera_pivot_1 = graph
            .find_by_name_from_root("Camera - Pivot - 1").unwrap().0;
        let camera_pivot_2 = graph
            .find_by_name_from_root("Camera - Pivot - 2").unwrap().0;
        let auto_aim_cone = graph.find_by_name(
            camera_pivot_2,
            "Auto Aim Detector Cone Collider")
            .unwrap().0;
        let auto_aim_ray_end = graph.find_by_name(
            camera_pivot_2,
            "Auto Aim Ray Cast End")
            .unwrap().0;
        let player = graph
            .find_by_name_from_root("Player Character - RB").unwrap().0;
        let arrow_anchor = graph.find_by_name(
            player,
            "Arrow Anchor Point")
            .unwrap().0;
        let preloaded_arrow =
            resource_manager
            .request(
                "./data/vanilla/models/arrow/arrow.rgs");
        let shooting_msg = ShootingMsg::ShootingData {
            arrow_yaw: 0.0,
            arrow_pitch: 0.0,
            init_spd: 0.0,
        };

        let averaged_draw_weight_rcv = Arc::new(Mutex::new(15.0));
        let averaged_draw_weight_snd = Arc::clone(&averaged_draw_weight_rcv);
        let current_draw_weight_rcv = Arc::new(Mutex::new(-1.0));
        let current_draw_weight_snd = Arc::clone(&current_draw_weight_rcv);
        let last_draw_time_rcv = Arc::new(Mutex::new(0.0));
        let last_draw_time_snd = Arc::clone(&last_draw_time_rcv);
        let bow_weight_rcv = Arc::new(Mutex::new(25.0));
        let bow_weight_snd = Arc::clone(&bow_weight_rcv);
        let current_hold_over_rcv = Arc::new(Mutex::new(1.0));
        let current_hold_over_snd = Arc::clone(&current_hold_over_rcv);
        let avg_hold_over_rcv = Arc::new(Mutex::new(1.0));
        let avg_hold_over_snd = Arc::clone(&avg_hold_over_rcv);
            
        thread::spawn(move || {
			shooting_ability_loop(averaged_draw_weight_snd,
                                  current_draw_weight_rcv,
                                  last_draw_time_rcv,
                                  bow_weight_rcv,
                                  avg_hold_over_snd,
                                  current_hold_over_rcv)
		});

        let player_control = PlayerControl {
            mouse_sensitivity: 0.01,
            mouse_vir_x_0: 0.0,
            mouse_vir_y_0: 0.0,
            mouse_vir_x: 0.0,
            mouse_vir_y: 0.0,
            key_pressed: Default::default(),
            combat_mode: CombatMode::NonCombat,
            cam_pitch: 0.0,
            cam_yaw: 0.0,
            camera_root: camera_root,
            camera_pivot_1: camera_pivot_1,
            camera_pivot_2: camera_pivot_2,
            auto_aim_cone: auto_aim_cone,
            auto_aim_ray_end: auto_aim_ray_end,
            player: player,
            player_yaw: 0.0,
            yaw_want_in_vec: [0.0, 0.0],
            yaw_want: 0.0,
            walk_speed: 9.0,
            target_to_aim: None,
            arrow_anchor: arrow_anchor,
            preloaded_arrow: Some(preloaded_arrow),
            yaw_to_aim: 0.0,
            focus: false,
            bow_weight_snd: bow_weight_snd,
            current_draw_weight_snd: current_draw_weight_snd,
            last_draw_time_snd: last_draw_time_snd,
            averaged_draw_weight_rcv: averaged_draw_weight_rcv,
            avg_hold_over_rcv: avg_hold_over_rcv,
            current_hold_over_snd: current_hold_over_snd,
            bow_start_drawing: false,
            current_draw_length: 0.0,
            shooting_msg: shooting_msg,
            bow_reload_timer: 0.0,
            bow_reload_timer_start: false,
            pre_melee_attack_from: Default::default(),
        };
        
        player_control
    }

    fn camera_to_player(&mut self, graph: &mut Graph) {
        let player_position
            = *graph[self.player].local_transform().position().get_value_ref();
        graph[self.camera_root].local_transform_mut()
            .set_position(player_position);
    }
    
    fn mouse_move(&mut self, event: &DeviceEvent) {
        match event {
            DeviceEvent::MouseMotion { delta } => {
                let (mouse_delta_x, mouse_delta_y) = delta;
                self.switch_melee_attack_direction(*mouse_delta_x,
                                                   *mouse_delta_y * -1.0);
                self.mouse_vir_x += *mouse_delta_x;
                self.mouse_vir_y += *mouse_delta_y;
                if self.focus == false {
                    self.cam_yaw = self.cam_yaw + *mouse_delta_x as f32
                        * self.mouse_sensitivity;
                    if self.cam_yaw < 0.0 {
                        self.cam_yaw = 360.0f32.to_radians()
                            + self.cam_yaw % 360.0f32.to_radians();
                    } else {
                        self.cam_yaw = self.cam_yaw % 360.0f32.to_radians();
                    }
                    self.cam_pitch = (self.cam_pitch - *mouse_delta_y as f32
                                      * self.mouse_sensitivity)
                        .max(-90.0f32.to_radians())
                        .min(90.0f32.to_radians());
                }
            },
            _=> {},
        }
    }

    fn switch_melee_attack_direction (&mut self,
                                      x_delta: f64,
                                      y_delta: f64) {
        let rad = vec_to_rad_f64([x_delta, y_delta]);
        if rad >= 0.0 && rad < 10.0f64.to_radians()
            || rad > 350.0f64.to_radians() && rad < 0.0
        {
            self.pre_melee_attack_from = PrepareMeleeAttackFrom::Top;
        } else if rad >= 10.0f64.to_radians() && rad < 90.0f64.to_radians() {
            self.pre_melee_attack_from = PrepareMeleeAttackFrom::TopRight;
        } else if rad >= 90.0f64.to_radians() && rad < 170.0f64.to_radians() {
            self.pre_melee_attack_from = PrepareMeleeAttackFrom::BottomRight;
        } else if rad >= 170.0f64.to_radians() && rad < 190.0f64.to_radians() {
            self.pre_melee_attack_from = PrepareMeleeAttackFrom::Back;
        } else if rad >= 190.0f64.to_radians() && rad < 270.0f64.to_radians() {
            self.pre_melee_attack_from = PrepareMeleeAttackFrom::BottomLeft;
        } else {
            self.pre_melee_attack_from = PrepareMeleeAttackFrom::TopLeft;
        }
        // println!("{} {} {}", x_delta, y_delta, rad);
        // println!("{:?}", self.pre_melee_attack_from);
    }
    
    fn camera_pivot_move(&mut self, graph: &mut Graph){
        let multiplied_quaternion_angle =
            UnitQuaternion::from_axis_angle(
                &Vector3::y_axis(),
                360.0f32.to_radians() - self.cam_yaw,
            ) 
            * UnitQuaternion::from_axis_angle(
                &Vector3::x_axis(),
                self.cam_pitch,
            );
        graph[self.camera_pivot_1]
            .local_transform_mut()
            .set_rotation(multiplied_quaternion_angle);
    }
    
    fn update_direction_keys(&mut self, window_event: &WindowEvent) {
        match window_event {
            WindowEvent::KeyboardInput {event, ..} => {
                let phy_key = event.physical_key;
                let key_state = event.state;
                match phy_key {
                    PhysicalKey::Code(key_code) => {
                        match key_state {
                            ElementState::Pressed => {
                                match key_code {
                                    KeyCode::KeyW => {
                                        self.key_pressed.w = true;
                                    },
                                    KeyCode::KeyA => {
                                        self.key_pressed.a = true;
                                    },
                                    KeyCode::KeyS => {
                                        self.key_pressed.s = true;
                                    },
                                    KeyCode::KeyD => {
                                        self.key_pressed.d = true;
                                    },
                                    _=> {},
                                }
                            },
                            _=>{
                                match key_code {
                                    KeyCode::KeyW => {
                                        self.key_pressed.w = false;
                                    },
                                    KeyCode::KeyA => {
                                        self.key_pressed.a = false;
                                    },
                                    KeyCode::KeyS => {
                                        self.key_pressed.s = false;
                                    },
                                    KeyCode::KeyD => {
                                        self.key_pressed.d = false;
                                    },
                                    _=> {},
                                }
                            },
                        }
                    },
                    _=> {},
                }
            },
            _=> {},
        }
    }

    fn dir_key_to_yaw_want(&mut self, dt: f32) {
        let at = 0.3;
        if self.key_pressed.w && self.key_pressed.s == false {
            self.yaw_want_in_vec[1] += 1.0 / at * dt;
            if 1.0 - self.yaw_want_in_vec[1] <= 1.0 / at * dt {
                self.yaw_want_in_vec[1] = 1.0;
            }
        } else if self.key_pressed.w == false && self.key_pressed.s {
            self.yaw_want_in_vec[1] -= 1.0 / at * dt;
            if self.yaw_want_in_vec[1] - 1.0 <= 1.0 / at * dt {
                self.yaw_want_in_vec[1] = -1.0;
            }
        } else if self.key_pressed.w == false && self.key_pressed.s == false {
            if self.yaw_want_in_vec[1].abs() <= 1.0 / at * dt {
                self.yaw_want_in_vec[1] = 0.0;
            } else if self.yaw_want_in_vec[1] < 0.0 {
                self.yaw_want_in_vec[1] += 1.0 / at * dt;
            } else if self.yaw_want_in_vec[1] > 0.0 {
                self.yaw_want_in_vec[1] -= 1.0 / at * dt;
            }
        }
        if self.key_pressed.d && self.key_pressed.a == false {
            self.yaw_want_in_vec[0] += 1.0 / at * dt;
            if 1.0 - self.yaw_want_in_vec[0] <= 1.0 / at * dt {
                self.yaw_want_in_vec[0] = 1.0;
            }
        } else if self.key_pressed.d == false && self.key_pressed.a {
            self.yaw_want_in_vec[0] -= 1.0 / at * dt;
            if self.yaw_want_in_vec[0] - 1.0 <= 1.0 / at * dt {
                self.yaw_want_in_vec[0] = -1.0;
            }
        } else if self.key_pressed.d == false && self.key_pressed.a == false {
            if self.yaw_want_in_vec[0].abs() <= 1.0 / at * dt {
                self.yaw_want_in_vec[0] = 0.0;
            } else if self.yaw_want_in_vec[0] < 0.0 {
                self.yaw_want_in_vec[0] += 1.0 / at * dt;
            } else if self.yaw_want_in_vec[0] > 0.0 {
                self.yaw_want_in_vec[0] -= 1.0 / at * dt;
            }
        }
        self.yaw_want = vec_to_rad(self.yaw_want_in_vec);
    }
    
    fn player_turn(&mut self, graph: &mut Graph, dt: f32) {

        let control_activated = distance_2d([0.0, 0.0],
                                            self.yaw_want_in_vec) > 0.0;
        if control_activated {
            let rad_per_sec = 720.0f32.to_radians() * dt;
            let cam_yaw_want
                = (self.cam_yaw + self.yaw_want)
                % 360.0f32.to_radians();
            let mut rad_to_turn = cam_yaw_want - self.player_yaw;
            if rad_to_turn > 180.0f32.to_radians() {
                rad_to_turn -= 360.0f32.to_radians();
            }
            if rad_to_turn < -180.0f32.to_radians() {
                rad_to_turn += 360.0f32.to_radians();
            }
            if rad_to_turn > rad_per_sec / 1.8 {
                self.player_yaw += rad_per_sec;
            }
            if rad_to_turn < - rad_per_sec / 1.8 {
                self.player_yaw -= rad_per_sec;
            }
            if rad_to_turn.abs() <= rad_per_sec / 1.8 {
                self.player_yaw += rad_to_turn;
            }
            graph[self.player]
                .as_rigid_body_mut()
                .local_transform_mut()
                .set_rotation(UnitQuaternion::from_axis_angle(
                    &Vector3::y_axis(),
                    360.0f32.to_radians() - self.player_yaw,
                ));
        }
    }
    
    fn player_walk(&mut self, graph: &mut Graph, dt: f32) {
        let player_rb = graph[self.player].as_rigid_body_mut();
        let vs = player_rb.lin_vel().y;
        let speed = self.walk_speed * dt;
        let look_vector = player_rb
            .look_vector()
            .try_normalize(f32::EPSILON)
            .unwrap_or(Vector3::z());
        let walk_vec = normalize_2d(self.yaw_want_in_vec);
        let mut walk_vel = Vector3::default();
        walk_vel = walk_vel + look_vector * -walk_vec[1].abs() * speed;
        walk_vel = walk_vel + look_vector * -walk_vec[0].abs() * speed;
        player_rb.set_lin_vel(Vector3::new(
            walk_vel.x / dt,
            vs,
            walk_vel.z / dt,
        ));    
    }

    fn find_auto_aim_target(&mut self, graph: &Graph) {
        let cam_piv_2 = self.camera_pivot_2;
        let cam_piv_2_pos = graph[cam_piv_2].global_position();
        let mut ray_buf = ArrayVec::<Intersection, 32>::new();
        let ray_end_pos = graph[self.auto_aim_ray_end]
            .global_position();
        let ray_dir = ray_end_pos - cam_piv_2_pos;
        let ray_len = distance_3d(ray_end_pos, cam_piv_2_pos);
        graph.physics.cast_ray(
            RayCastOptions {
                ray_origin: Point3::from(cam_piv_2_pos),
                ray_direction: ray_dir,
                max_len: ray_len,
                groups: Default::default(),
                sort_results: true,
            },
            &mut ray_buf,
        );
        let auto_aim_cone = self.auto_aim_cone;
        let aacc = graph[auto_aim_cone].as_collider();
        let aacc_intersects = aacc.intersects(&graph.physics);
        let mut min_angle = 3.14159265;
        for intersect in aacc_intersects {
            let reg_col_handle1 = intersect.collider1;
            let reg_col_handle2 = intersect.collider2;
            let reg_col_handle: Handle<Node>;
            if graph[reg_col_handle1].name()
                == "Auto Aim Detector Cone Collider"
            {
                reg_col_handle = reg_col_handle2;
            } else {
                reg_col_handle = reg_col_handle1;
            }
            if graph[reg_col_handle].tag().contains("hitbox") {
                let hitbox_pos = graph[reg_col_handle]
                    .global_position();
                let cam_piv_2_pos = graph[cam_piv_2].global_position();
                let hitbox_dir = hitbox_pos - cam_piv_2_pos;
                let hitbox_dir = hitbox_dir.normalize();
                let cam_look = -graph[cam_piv_2].look_vector();
                let cam_look = cam_look.normalize();
                let target_angle = angle_vec_3d(cam_look, hitbox_dir);
                let target_dist = distance_3d(hitbox_pos, cam_piv_2_pos);
                if target_dist < 200.0 {
                    if target_angle < min_angle
                    {
                        min_angle = target_angle;
                        let mut is_hit_by_ray = false;
                        for i in ray_buf.iter() {
                            if i.collider== reg_col_handle {
                                is_hit_by_ray = true;
                                break;
                            }
                        }
                        if target_angle < 15.0f32.to_radians()
                            || is_hit_by_ray
                        {
                            self.target_to_aim = Some(reg_col_handle);
                        }
                    }
                }
            }
        }
    }


    fn auto_aim(&mut self, graph: &mut Graph, dt: f32) {
        let aa_target = self.target_to_aim;
        if aa_target.is_some() {
            let aa_handle = aa_target.unwrap();
            let hitbox_pos = graph[aa_handle].global_position();
            let player_pos = graph[self.player].global_position();
            let pl_hb_vec =  hitbox_pos - player_pos;
            let pl_hb_vec = pl_hb_vec.normalize();
            let pl_hb_sli = pl_hb_vec.as_slice();
            let pl_hb_yaw_in_vec = [pl_hb_sli[0], pl_hb_sli[2]];
            let pl_hb_yaw = vec_to_rad(pl_hb_yaw_in_vec);
            let player_look = -graph[self.player].look_vector();
            let player_look = player_look.normalize();
            let player_look_sli = player_look.as_slice();
            let player_look_yaw_in_vec = [player_look_sli[0],
                                          player_look_sli[2]];
            let player_look_yaw = vec_to_rad(player_look_yaw_in_vec);
            let yaw_to_aim = player_look_yaw - pl_hb_yaw;
            self.yaw_to_aim = yaw_to_aim;
            let control_activated =  self.yaw_to_aim != 0.0;
            if control_activated {
                let rad_per_sec = 720.0f32.to_radians() * dt;
                let mut rad_to_turn = self.yaw_to_aim;
                if rad_to_turn > 180.0f32.to_radians() {
                    rad_to_turn -= 360.0f32.to_radians();
                }
                if rad_to_turn < -180.0f32.to_radians() {
                    rad_to_turn += 360.0f32.to_radians();
                }
                if rad_to_turn > rad_per_sec / 1.8 {
                    self.player_yaw += rad_per_sec;
                }
                if rad_to_turn < - rad_per_sec / 1.8 {
                    self.player_yaw -= rad_per_sec;
                }
                if rad_to_turn.abs() <= rad_per_sec / 1.8 {
                    self.player_yaw += rad_to_turn;
                    self.yaw_to_aim = 0.0;
                }
                graph[self.player]
                    .as_rigid_body_mut()
                    .local_transform_mut()
                    .set_rotation(UnitQuaternion::from_axis_angle(
                        &Vector3::y_axis(),
                        360.0f32.to_radians() - self.player_yaw,
                    ));
            }
            
        }
    }

    fn do_auto_aim(&mut self, window_event: &WindowEvent, graph: &Graph) {
        match window_event {
            WindowEvent::MouseInput {state, button, .. } => {
                match button {
                    MouseButton::Right => {
                        match state {
                            ElementState::Pressed => {
                                println!("Right Mouse Press!");
                                self.find_auto_aim_target(graph);
                                self.focus = true;
                            },
                            _=> {
                                self.target_to_aim = None;
                                self.focus = false;
                            },
                        }
                    },
                    _=> {},
                }
            },
            _=> {},
        }
    }

    fn select_noncombat(&mut self,
                        graph: &mut Graph,) {
        match self.combat_mode {
            CombatMode::NonCombat => {
                println!("It is Non Combat Mode already!");
            },
            _=> {
                self.combat_mode = CombatMode::NonCombat;
                println!("Combat Mode: NonCombat");
                self.bow_unload(graph);
            }
        }
    }

    fn select_archery(&mut self,
                      graph: &mut Graph){
        match self.combat_mode {
            CombatMode::Archery => {
                println!("It is Archery Mode already!");
            },
            _=> {
                self.combat_mode = CombatMode::Archery;
                println!("Combat Mode: Archery");
                let arrow_rb = graph
                    .find_by_name(self.arrow_anchor,
                                  "Arrow Rigid Body");
                if arrow_rb.is_none() {
                    self.bow_reload_timer_start = true;
                }
            }
        }
    }

    fn select_melee(&mut self,
                    graph: &mut Graph){
        match self.combat_mode {
            CombatMode::Melee => {
                println!("It is Melee Mode already!");
            },
            _=> {
                self.combat_mode = CombatMode::Melee;
                println!("Combat Mode: Melee");
                self.bow_unload(graph);
            }
        }
    }

    fn change_combat_mode_key(&mut self,
                              graph: &mut Graph,
                              window_event: &WindowEvent) {
        match window_event {
            WindowEvent::KeyboardInput {event, ..} => {
                let phy_key = event.physical_key;
                let ks = event.state;
                match phy_key {
                    PhysicalKey::Code(kc) => {
                        match ks {                    
                            ElementState::Pressed => {
                                match kc {
                                    KeyCode::Digit1 => {
                                        if self.key_pressed.key1 == false {
                                            self.select_noncombat(graph);
                                        }
                                        self.key_pressed.key1 = true;
                                    },
                                    KeyCode::Digit2 => {
                                        if self.key_pressed.key2 == false {
                                            self.select_archery(graph);
                                        }
                                        self.key_pressed.key2 = true;
                                    },
                                    KeyCode::Digit3 => {
                                        if self.key_pressed.key3 == false {
                                            self.select_melee(graph);
                                        }
                                        self.key_pressed.key3 = true;
                                    },
                                    _=>{},
                                }
                            },
                            _=>{
                                match kc {
                                    KeyCode::Digit1 => {
                                        self.key_pressed.key1 = false;
                                    },
                                    KeyCode::Digit2 => {
                                        self.key_pressed.key2 = false;
                                    },
                                    KeyCode::Digit3 => {
                                        self.key_pressed.key3 = false;
                                    },
                                    _=>{},
                                }
                            },
                        }
                    },
                    _=> {},
                }
            },
            _=> {},
        }
    }

    fn do_bow_draw_start(&mut self, graph: &Graph) {
        self.find_auto_aim_target(graph);
        self.bow_start_drawing = true;
        self.mouse_vir_x_0 = self.mouse_vir_x;
        self.mouse_vir_y_0 = self.mouse_vir_y;
        self.current_draw_length = 0.0;
        let mut current_hold_over_snd = self.current_hold_over_snd
            .lock().unwrap();
        *current_hold_over_snd = 0.0;
    }

    fn do_bow_draw(&mut self,
                   graph: &mut Graph,
                   dt: f32) {
        let sen = self.mouse_sensitivity as f64;
        let dx = self.mouse_vir_x - self.mouse_vir_x_0;
        let dy = self.mouse_vir_y - self.mouse_vir_y_0;
        if self.bow_start_drawing {
            let dl = (dx.powf(2.0) + dy.powf(2.0)).sqrt() * sen;
            let std_draw_len = 28.0;
            let brace_h = 7.5;
            let arrow_weight = 0.0281875;
            let bow_weight = *self.bow_weight_snd.lock().unwrap();
            let avg_weight = *self.averaged_draw_weight_rcv
                .lock().unwrap();
            let k_factor = lbs_newton(bow_weight) / inch_meter(std_draw_len);
            let max_dl = lbs_newton(avg_weight) / k_factor;
            let inches_per_dl = (std_draw_len - brace_h) / 1.5;
            let mut max_dl_inches = meter_inch(max_dl);
            if max_dl_inches > std_draw_len + 2.0 {
                max_dl_inches = std_draw_len + 2.0
            }            
            self.current_draw_length = dl * inches_per_dl + brace_h;
            if self.current_draw_length >  max_dl_inches + 1.0 {
                self.current_draw_length = max_dl_inches + 1.0;
            }
            let draw_meters = inch_meter(self.current_draw_length);
            let brace_h_meters = inch_meter(brace_h);
            let energy_stored = 0.5 * k_factor * draw_meters.powf(2.0);
            let energy_left = 0.5 * k_factor * brace_h_meters.powf(2.0);
            let energy_released = energy_stored - energy_left;
            let init_spd = (2.0 * energy_released / arrow_weight).sqrt();
            let gravity = -graph.physics.gravity[1];
            let target_relative_x: f32;
            let target_relative_y: f32;
            let target = self.target_to_aim;
            let arrow_yaw: f32;
            if target.is_some() {
                let target_handle = target.unwrap();
                let tx = graph[target_handle].global_position()[0];
                let ty = graph[target_handle].global_position()[1];
                let tz = graph[target_handle].global_position()[2];
                let ax = graph[self.arrow_anchor].global_position()[0];
                let ay = graph[self.arrow_anchor].global_position()[1];
                let az = graph[self.arrow_anchor].global_position()[2];
                target_relative_x = distance_2d([tx, tz], [ax, az]);
                target_relative_y = ty - ay;

                let target_pos = graph[target_handle].global_position();
                let anchor_pos = graph[self.arrow_anchor].global_position();
                let ta_vec = target_pos - anchor_pos;
                let ta_vec = ta_vec.normalize();
                let ta_vec = ta_vec.as_slice();
                let ta_vec_yaw = [ta_vec[0], ta_vec[2]];
                let ta_yaw = vec_to_rad(ta_vec_yaw);
                arrow_yaw = 180.0f32.to_radians() - ta_yaw;
            } else {
                target_relative_x = 15.0;
                target_relative_y = 0.0;

                arrow_yaw = self.player_yaw;
            }
            let arrow_anchor_pitch =  projectile_angle(target_relative_x,
                                                       target_relative_y,
                                                       init_spd as f32,
                                                       gravity).0;
            let arrow_anchor_yaw_loc = self.player_yaw - arrow_yaw;
            let arrow_quaternion_angle =
                UnitQuaternion::from_axis_angle(
                    &Vector3::y_axis(),
                    arrow_anchor_yaw_loc,
                )
                * UnitQuaternion::from_axis_angle(
                    &Vector3::x_axis(),
                    arrow_anchor_pitch,
                );
            
            graph[self.arrow_anchor]
                .local_transform_mut()
            .set_rotation(arrow_quaternion_angle);

            self.shooting_msg = ShootingMsg::ShootingData {
                arrow_yaw: arrow_yaw,
                arrow_pitch: arrow_anchor_pitch,
                init_spd: init_spd as f32,
            };
            
            if self.current_draw_length > brace_h {
                let mut current_hold_over_snd = self.current_hold_over_snd
                    .lock().unwrap();
                *current_hold_over_snd += dt as f64;
            }
        }
    }

    fn do_bow_release(&mut self,
                      message_sender: &ScriptMessageSender) {
        self.target_to_aim = None;
        let std_draw_len = 28.0;
        let bow_weight = *self.bow_weight_snd.lock().unwrap();
        let k_factor = lbs_newton(bow_weight) / inch_meter(std_draw_len);
        let draw_inches = self.current_draw_length;
        let draw_meters = inch_meter(draw_inches);
        let weight_holding = newton_lbs(draw_meters * k_factor);
        let mut crn_draw_wght_snd = self.current_draw_weight_snd
            .lock().unwrap();
        *crn_draw_wght_snd = weight_holding;
        let mut last_draw_time_snd = self.last_draw_time_snd
            .lock().unwrap();
        *last_draw_time_snd = unix_time_secs();
        self.bow_start_drawing = false;

        let max_yaw_err_deg_by_spine = 5.0;
        let yaw_err_fac_by_spine = (bow_weight - weight_holding) / bow_weight;
        let yaw_err_deg_by_spine
            = max_yaw_err_deg_by_spine * yaw_err_fac_by_spine;
        let yaw_err_by_spine = yaw_err_deg_by_spine.to_radians();

        let max_err_deg_by_hold = 3.0;
        let avg_hold = *self.avg_hold_over_rcv.lock().unwrap();
        let current_hold = *self.current_hold_over_snd.lock().unwrap();
        let delta_hold = current_hold - avg_hold;
        let factor_hold = 1.0;
        let mut err_deg_by_hold = delta_hold * factor_hold;
        if delta_hold > 0.0 {
            if err_deg_by_hold >= max_err_deg_by_hold {
                err_deg_by_hold = max_err_deg_by_hold;
            }
        } else {
            err_deg_by_hold = 0.0;
        }
        let mut rand_plus_min_one = random::<f64>();
        if random() {
            rand_plus_min_one = -rand_plus_min_one;
        }
        let pitch_err_deg_by_hold = err_deg_by_hold * rand_plus_min_one;
        let mut rand_plus_min_one = random::<f64>();
        if random() {
            rand_plus_min_one = -rand_plus_min_one;
        }
        let yaw_err_deg_by_hold = err_deg_by_hold * rand_plus_min_one;
        let pitch_err_by_hold = pitch_err_deg_by_hold.to_radians();
        let yaw_err_by_hold = yaw_err_deg_by_hold.to_radians();
        
        if let ShootingMsg::ShootingData{ref mut arrow_yaw,
                                         ref mut arrow_pitch,
                                         ..} = self.shooting_msg {
            *arrow_yaw += yaw_err_by_spine as f32;
            *arrow_yaw += yaw_err_by_hold as f32;
            *arrow_pitch += pitch_err_by_hold as f32;
        }
        
        message_sender.send_global(
            self.shooting_msg.clone(),
        );
    }

    fn bow_reload(&mut self,
                  scene: &mut Scene,
                  dt: f32){
        if self.bow_reload_timer_start {
            self.bow_reload_timer += dt;
            if self.bow_reload_timer >= 1.0 {
                let new_arrow = self.preloaded_arrow.clone().unwrap()
                    .instantiate(scene);
                let graph = &mut scene.graph;
                graph.link_nodes(new_arrow, self.arrow_anchor);
                self.bow_reload_timer_start = false;
                self.bow_reload_timer = 0.0;
            }
        }
    }

    fn bow_unload(&mut self,
                  graph: &mut Graph){
        let arrow_rb = graph.find_by_name(self.arrow_anchor
                                          , "Arrow Rigid Body");
        if arrow_rb.is_some() {
            graph.remove_node(arrow_rb.unwrap().0);
        }
    }

    fn bow_draw_release(&mut self,
                        graph: &mut Graph,
                        window_event: &WindowEvent,
                        message_sender: &ScriptMessageSender) {
        match window_event {
            WindowEvent::MouseInput {state, button, .. } => {
                match button {
                    MouseButton::Left => {
                        match state {
                            ElementState::Pressed => {
                                match self.combat_mode {
                                    CombatMode::Archery => {
                                        self.do_bow_draw_start(graph);
                                    },
                                    _=> {},
                                }
                            },
                            ElementState::Released => {
                                match self.combat_mode {
                                    CombatMode::Archery => {
                                        self.do_bow_release(message_sender);
                                        self.bow_reload_timer_start = true;
                                    },
                                    _=> {},
                                }
                            },
                        }
                    },
                    _=> {},
                }
            },
            _=> {},
        }
    }
}

impl ScriptTrait for PlayerControl {

    fn on_init(& mut self, context: &mut ScriptContext) {
        // Put initialization logic here.
        *self = self.new(&mut context.scene.graph, context.resource_manager);
    }

    fn on_start(&mut self, _context: &mut ScriptContext) {
        // There should be a logic that depends on other scripts in scene.
        // It is called right after **all** scripts were initialized.
    }

    fn on_deinit(&mut self, _context: &mut ScriptDeinitContext) {
        // Put de-initialization logic here.
    }
    
    fn on_os_event(&mut self, event: &Event<()>, context: &mut ScriptContext) {
        // Respond to OS events here.
        match event {
            Event::DeviceEvent { event, .. } => {
                let game_plugin = context.plugins[0].cast::<Game>().unwrap();
                let menu_on = game_plugin.menu_on;
                if menu_on == false {
                    self.camera_pivot_move(&mut context.scene.graph);
                    self.mouse_move(event);
                }
            },
            Event::WindowEvent {event, .. } => {
                let game_plugin = context.plugins[0].cast::<Game>().unwrap();
                let menu_on = game_plugin.menu_on;
                if menu_on == false {
                    self.update_direction_keys(event);
                    self.do_auto_aim(event, &mut context.scene.graph);
                    self.bow_draw_release(&mut context.scene.graph,
                                          event,
                                          &context.message_sender);
                    self.change_combat_mode_key(
                        &mut context.scene.graph,
                        event);
                }
            },
            _=> {}
        }
    }

    fn on_update(&mut self, context: &mut ScriptContext) {
        // Put object logic here.
        // context.scene.drawing_context.clear_lines();
        // context.scene.graph.physics.draw(&mut context.scene.drawing_context);
        self.camera_to_player(&mut context.scene.graph);
        self.dir_key_to_yaw_want(context.dt);
        self.player_turn(&mut context.scene.graph, context.dt);
        self.player_walk(&mut context.scene.graph, context.dt);
        self.auto_aim(&mut context.scene.graph, context.dt);
        self.do_bow_draw(&mut context.scene.graph,
                         context.dt);
        self.bow_reload(&mut context.scene,
                        context.dt);
    }
}
    
