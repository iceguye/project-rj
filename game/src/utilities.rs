//  Copyright © 2021 - 2023 IceGuye.

//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, version 3 of the
//  License.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <http://www.gnu.org/licenses/>.

use std::time::{SystemTime, UNIX_EPOCH};
use fyrox::core::algebra::base::Vector3;

pub fn unix_time_secs() -> f64 {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    let in_secs: f64 = since_the_epoch.as_secs_f64();
    in_secs
}

pub fn vec_to_rad(vec: [f32;2]) -> f32 {
    let mut rad:f32 = 0.0;
    if vec[0] != 0.0 || vec[1] != 0.0 {
        rad = (vec[0] / vec[1])
            .atan();            
    }
    if vec[0] >= 0.0 && vec[1] < 0.0 {
        rad
            = 90.0f32.to_radians()
            + rad
            + 90.0f32.to_radians();
    }
    if vec[0] < 0.0 && vec[1] < 0.0 {
        rad = rad + 180.0f32.to_radians();
    }
    if vec[0] < 0.0 && vec[1] >= 0.0 {
        rad
            = 90.0f32.to_radians()
            + rad
            + 270.0f32.to_radians()
    }
    rad
}

pub fn vec_to_rad_f64(vec: [f64;2]) -> f64 {
    let mut rad:f64 = 0.0;
    if vec[0] != 0.0 || vec[1] != 0.0 {
        rad = (vec[0] / vec[1])
            .atan();            
    }
    if vec[0] >= 0.0 && vec[1] < 0.0 {
        rad
            = 90.0f64.to_radians()
            + rad
            + 90.0f64.to_radians();
    }
    if vec[0] < 0.0 && vec[1] < 0.0 {
        rad = rad + 180.0f64.to_radians();
    }
    if vec[0] < 0.0 && vec[1] >= 0.0 {
        rad
            = 90.0f64.to_radians()
            + rad
            + 270.0f64.to_radians()
    }
    rad
}

pub fn normalize_2d(vec: [f32; 2]) -> [f32; 2] {
    let rad = vec_to_rad(vec);
    [rad.sin() * vec[0], rad.cos() * vec[1]]
}

pub fn distance_2d(point1: [f32; 2], point2: [f32; 2]) -> f32 {
    let pow_x = (point2[0] - point1[0]).powf(2.0);
    let pow_y = (point2[1] - point1[1]).powf(2.0);
    let distance = (pow_x + pow_y).sqrt();
    distance
}

pub fn distance_3d(end: Vector3<f32>, begin: Vector3<f32>) -> f32 {
    let end_sli = end.as_slice();
    let begin_sli = begin.as_slice();
    let end_x = end_sli[0];
    let end_y = end_sli[1];
    let end_z = end_sli[2];
    let begin_x = begin_sli[0];
    let begin_y = begin_sli[1];
    let begin_z = begin_sli[2];
    let pow_x = (end_x - begin_x) * (end_x - begin_x);
    let pow_y = (end_y - begin_y) * (end_y - begin_y);
    let pow_z = (end_z - begin_z) * (end_z - begin_z);
    let distance = (pow_x + pow_y + pow_z).sqrt();
    
    distance
}

pub fn angle_vec_3d(vec1: Vector3<f32>, vec2: Vector3<f32>) -> f32 {
    let vec1 = vec1.normalize();
    let vec2 = vec2.normalize();    
    let vec1 = vec1.as_slice();
    let vec2 = vec2.as_slice();
    let vec1_2 = vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2];
    let vec1_abs = (vec1[0] * vec1[0] + vec1[1] * vec1[1] + vec1[2] * vec1[2])
        .sqrt();
    let vec2_abs = (vec2[0] * vec2[0] + vec2[1] * vec2[1] + vec2[2] * vec2[2])
        .sqrt();
    let cos_a = vec1_2 / (vec1_abs * vec2_abs);
    let angle = cos_a.acos();

    angle
}

pub fn projectile_angle(x: f32, y: f32, v0: f32, g: f32) -> (f32, f32) {
    let a = (-0.5) * g * x.powf(2.0) / v0.powf(2.0);
    let b = x;
    let c = - (y-a);
    let bp_4ac = b.powf(2.0) - 4.0 * a * c;
    if bp_4ac < 0.0 {
        // Out of range or too high. If out if range and still want to
        // try, try 45 degrees; if too high and still want to try, try
        // 89 degrees.
        if x > y {
            (45.0f32.to_radians(), 45.0f32.to_radians())
        } else {
            (89.0f32.to_radians(), 89.0f32.to_radians())
        }
    } else {
        let theta1 = ((-b + bp_4ac.sqrt()) / (2.0 * a)).atan(); 
        let theta2 = ((-b - bp_4ac.sqrt()) / (2.0 * a)).atan();
        (theta1, theta2)
    }
}

pub fn inch_meter(inches: f64) -> f64 {
    let meter = inches * 0.0254;
    meter
}

pub fn meter_inch(meter: f64) -> f64 {
    meter / 0.0254
}

pub fn lbs_newton(lbs: f64) -> f64 {
    let newton = lbs * 4.4482216153;
    newton
}

pub fn newton_lbs(newton: f64) -> f64 {
    let lbs = newton / 4.4482216153;
    lbs
}
