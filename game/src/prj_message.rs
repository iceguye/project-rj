use fyrox::{
    core::{reflect::prelude::*,
           visitor::prelude::*,
    },
};

#[derive(Visit, Reflect, Default, Debug, Clone)]
pub enum ShootingMsg {
    #[default]
    NoData,
    ShootingData {
        arrow_yaw: f32,
        arrow_pitch: f32,
        init_spd: f32,
    },
}
