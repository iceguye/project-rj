//  Copyright © 2021, 2023 IceGuye.

//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, version 3 of the
//  License.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <http://www.gnu.org/licenses/>.

use fyrox::{
    core::{reflect::prelude::*,
           visitor::prelude::*,
    },
};
use fyrox::core::rand::random;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::{thread, time};

pub fn retainability(interval: f64) -> f64 {
    let interval = interval / 60.0;
    if interval >= 1.0 {
        1.84 / (interval.log10().powf(1.25) + 1.84)
    } else {
        1.0
    }
}

#[derive(Visit, Reflect, Default, Debug, Clone)]
pub struct ShootingAbility {
    pub capacity: u64,
    pub occurrence: Vec<f64>,
    pub draw_weight: Vec<f64>,
    pub retained_draw_weight: f64,
    pub current_draw_weight: f64,
    pub last_draw_time: f64,
    pub hold_over_time: Vec<f64>,
    pub current_hold_over: f64,
    pub retained_hold_over: f64,
    pub bow_weight: f64, 
}

pub struct MeleeAbility {
    pub capacity: u64,
    // HashMap structure: {"Style1": [[time1, time2], [time1]],
    //                     "Style2": [[time1, time2, time3]]}
    pub melee_style_skill: HashMap<String, Vec<Vec<f64>>>,
    pub combat_rhythm: HashMap<String, Vec<Vec<f64>>>,
    pub retained_average: HashMap<String, Vec<f64>>,
}

pub trait UseAbility {
    fn new() -> Self;
    fn gain(&mut self);
    fn retain(&mut self);
}

impl UseAbility for ShootingAbility {
    fn new () -> ShootingAbility {
        ShootingAbility {
            occurrence: Vec::new(),
            draw_weight: Vec::new(),
            hold_over_time: Vec::new(),
            capacity: 9999999,
            current_draw_weight: -1.0,
            last_draw_time: 0.0,
            retained_draw_weight: 15.0,
            current_hold_over: 0.0,
            retained_hold_over: 0.0,
            bow_weight: 25.0
        }
    }
    
    fn gain(&mut self) {
        if self.occurrence.len() >= self.capacity as usize {
            let rm_len = self.occurrence.len() - self.capacity as usize;
            let mut i = 0;
            while i < rm_len {
                self.occurrence.remove(0);
                self.draw_weight.remove(0);
                self.hold_over_time.remove(0);
                i = i + 1;
            }
        }
        self.occurrence.push(self.last_draw_time);
        self.draw_weight.push(self.current_draw_weight);
        self.hold_over_time.push(self.current_hold_over);
    }
    
    fn retain(&mut self) {
        let current_time: f64 = self.last_draw_time;
        // Start shooting ability calculation.
        let mut shooting_sum: f64 = 0.0;
        let mut shooting_use: usize = 0;
        let mut hold_over_sum: f64 = 0.0;
        let mut hold_over_use: usize = 0;
        let occurrence = &self.occurrence;
        let shooting_len = occurrence.len();
        let draw_weight = &self.draw_weight;
        let hold_over = &self.hold_over_time;
        let mut wc = 0;
        while wc < shooting_len {
            let occurred_time = occurrence[wc];
            let interval = current_time - occurred_time;
            let retain = retainability(interval);
            let forget: f64 = random();
            if retain > forget{
                shooting_sum = shooting_sum + draw_weight[wc];
                shooting_use = shooting_use + 1;
                if draw_weight[wc] > self.bow_weight
                    && draw_weight[wc] > self.retained_draw_weight {
                        hold_over_sum += hold_over[wc];
                        hold_over_use += 1;
                    }
            }
            wc = wc + 1;
        }
        if shooting_use != 0 {
            self.retained_draw_weight = shooting_sum / (shooting_use as f64);
        } else if draw_weight.len() > 0 {
            let usize_rand = random::<usize>() % (draw_weight.len());
            self.retained_draw_weight = draw_weight[usize_rand];
        }
        if hold_over_use != 0 {
            self.retained_hold_over = hold_over_sum / hold_over_use as f64;
        } else if hold_over.len() > 0 {
            let usize_rand = random::<usize>() % (hold_over.len());
            self.retained_hold_over = hold_over[usize_rand];
        }
    }
}

// Ability infinite loop. This loop will continually and periodically
// calculate the abilities retained averages. This function should
// only be called once at the beginning of the game.
pub fn shooting_ability_loop (retained_draw_weight_snd: Arc<Mutex<f64>>,
                              current_draw_weight_rcv: Arc<Mutex<f64>>,
                              last_draw_time_rcv: Arc<Mutex<f64>>,
                              bow_weight_rcv: Arc<Mutex<f64>>,
                              avg_hold_over_snd: Arc<Mutex<f64>>,
                              current_hold_over_rcv: Arc<Mutex<f64>>) {
    
    let mut shooting_ability = ShootingAbility::new();
    
    loop {
        shooting_ability.bow_weight = *bow_weight_rcv.lock().unwrap();
        let last_draw_time = *last_draw_time_rcv.lock().unwrap();
        if last_draw_time > shooting_ability.last_draw_time {
            shooting_ability.last_draw_time = last_draw_time;
            let current_draw_weight = *current_draw_weight_rcv.lock().unwrap();
            if current_draw_weight > shooting_ability.retained_draw_weight {
                shooting_ability.current_draw_weight
                    = current_draw_weight + 0.3;
            } else {
                shooting_ability.current_draw_weight = current_draw_weight;
            }
            let bow_weight = *bow_weight_rcv.lock().unwrap();
            let current_hold_over = *current_hold_over_rcv.lock().unwrap();
            if current_draw_weight > bow_weight
                && current_draw_weight > shooting_ability.retained_draw_weight {
                    shooting_ability.current_hold_over
                        = current_hold_over + 0.1;
                } else {
                    shooting_ability.current_hold_over
                        = current_hold_over;
                }
            shooting_ability.gain();
            shooting_ability.retain();
            let mut draw_weight_update =
                retained_draw_weight_snd.lock().unwrap();
            *draw_weight_update = shooting_ability.retained_draw_weight;
            let mut hold_over_update = avg_hold_over_snd.lock().unwrap();
            *hold_over_update = shooting_ability.retained_hold_over;
            // println!("Test Slow Loop: {:?}", shooting_ability);
        }
        // This is a slow loop, and it will only run every 1 seconds
        // (1000 millis).
        let sleep_time = time::Duration::from_millis(1000);
        thread::sleep(sleep_time);
    }
}
